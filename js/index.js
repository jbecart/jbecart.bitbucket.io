 function nameFunction() {
	event.preventDefault();
    $('form').fadeOut(500);
	$('.wrapper').addClass('form-success');
	
	 setTimeout(function(){
		window.location.replace("play.html");
	 }, 500);

    localStorage.clear();
	localStorage.setItem("first_player_name", first_player.value);
	localStorage.setItem("second_player_name", second_player.value);
	localStorage.setItem("count", 1);
	jQuery.ajax({
    type: "POST",
    url: 'names.php',
    dataType: 'json',
    data: {functionname: 'names', arguments: [first_player.value,second_player.value]},

    success: function (obj, textstatus) {
                  if( !('error' in obj) ) {
                      names = obj.result;
					  console.log(names);
                  }
                  else {
                      console.log(obj.error);
                  }
            }
	});
}

if (localStorage.getItem("first_player_name") != null) {
	var check = parseInt(localStorage.getItem("count"));
	if (check % 2 == 0) {
		document.getElementById('player_id').innerHTML = localStorage.getItem("second_player_name") + "'s Move (x)";
	}
	else {
		document.getElementById('player_id').innerHTML = localStorage.getItem("first_player_name") + "'s Move (o)";
	}

	localStorage.setItem("count", 	check + 1);
	
}

 function playFunction() {
	event.preventDefault();
	$('form').fadeOut(500);
	$('.wrapper').addClass('form-success');
 
	setTimeout(function(){
		window.location.replace("play.html");
	}, 500);
	
	jQuery.ajax({
    type: "POST",
    url: 'move.php',
    dataType: 'json',
    data: {functionname: 'move', arguments: [row.value,column.value,player.value]},

    success: function (obj, textstatus) {
                  if( !('error' in obj) ) {
                      move = obj.result;
  					  console.log(move);
                  }
                  else {
                      console.log(obj.error);
                  }
            }
	});	
}

 function resetFunction() {
	event.preventDefault();
	$('form').fadeOut(500);
	$('.wrapper').addClass('form-success');
	setTimeout(function(){
		window.location.replace("player_name.html");
	}, 1000);
	
	localStorage.clear();
	
	jQuery.ajax({
    type: "POST",
    url: 'move.php',
    dataType: 'json',
    data: {functionname: 'delete'},
	});	
}