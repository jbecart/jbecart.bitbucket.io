<?php				
    header('Content-Type: application/json');

    $aResult = array();

    if( !isset($_POST['functionname']) ) { $aResult['error'] = 'No function name!'; }


    if( !isset($aResult['error']) ) {

        switch($_POST['functionname']) {
            case 'move':
                $aResult['result'] = [$_POST['arguments'][0],$_POST['arguments'][1],$_POST['arguments'][2]];
			   	$myfile = fopen("player_moves.txt", "a");
				$txt = $_POST['arguments'][0]  .  ","  .  $_POST['arguments'][1]  .  ","  .  $_POST['arguments'][2]  .  PHP_EOL;
				fwrite($myfile, $txt);
				fclose($myfile);
                break;            
				
			case 'delete':
			   	$myfile = fopen("player_moves.txt", "w");
				fclose($myfile);
                break;

            default:
               $aResult['error'] = 'Not found function '.$_POST['functionname'].'!';
               break;
        }

    }

    echo json_encode($aResult);

?>