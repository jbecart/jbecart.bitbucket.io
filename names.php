<?php
    header('Content-Type: application/json');

    $aResult = array();

    if( !isset($_POST['functionname']) ) { $aResult['error'] = 'No function name!'; }

    if( !isset($_POST['arguments']) ) { $aResult['error'] = 'No function arguments!'; }

    if( !isset($aResult['error']) ) {

        switch($_POST['functionname']) {
            case 'names':
				$aResult['result'] = [$_POST['arguments'][0],$_POST['arguments'][1]];
			   	$myfile = fopen("player_names.txt", "w");
				$txt = $_POST['arguments'][0]  .  ","  .  $_POST['arguments'][1];
				fwrite($myfile, $txt);
				fclose($myfile);
				//for clearing player moves
				$myfile = fopen("player_moves.txt", "w");
				fclose($myfile);
                break;

            default:
               $aResult['error'] = 'Not found function '.$_POST['functionname'].'!';
               break;
        }

    }

    echo json_encode($aResult);

?>