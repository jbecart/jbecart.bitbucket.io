const items = require('../games');

//test1
test('check if 0, 1 or 2 value', () => {
  for( i = 0; i < items.length ; i++) {
    var item = items[i];
    for( j = 0; j < item.length; j++ ) {
      expect(item[j]).toBeLessThan(3);
      expect(item[j]).toBeGreaterThanOrEqual(0);
    }
  }  
});
